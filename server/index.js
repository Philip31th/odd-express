const express = require('express')
const OddNotification = require('./odd.js');;
const app = express()
const port = 1337

const soxMonGC = {"id":"1577383745427","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:797af2cfdbd447d797bdaa71b1fd9bc3@thread.skype"}};
const bkMonGC = {"id":"1577383836738","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:5ad90cd5d6ee444b8a9cd45ed01d30e2@thread.skype"}};
const testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
const timeNotif = {"id":"1576602088454","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:9db73a306d534995a6aba39ccfb22d49@thread.skype"}};
const soxPaTeam = {"id":"f:d27fb1da-6a85-21c5-6714-691ba0251e9c","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:63bbc9f0f0cc46bb9531ab4e3442bf31@thread.skype"}};
const meetingsMon = {"id":"f:34a8adc8-3924-5a54-0597-944244248f16","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:0b393c50f882495fb6112b6d4f0a60bb@thread.skype"}};
const dl = {"id":"1574914343913","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6c841fe066df4537b3a67f9f9ae53d85@thread.skype"}};
const hd = {"id":"f:9aff401a-3cc9-e81c-8a30-3ab0c462346d","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:92d077d996884a88a4bbad4ede29d8d1@thread.skype"}};
const ns = {"id":"f:96da270f-d055-876d-52cd-8300c7a36c20","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:2bdc10b76a2e4e0e879757a23cd46c9e@thread.skype"}}
const soxTestingTimeNotification = {"id":"1579027517730","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:7f2c75427f6a442fa85908a229933313@thread.skype"}};
const bkTestingTimeNotification = {"id":"1579029482768","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:9998a661de4846a88aeeb0839e284180@thread.skype"}};
const dlAdmin = {"id":"f:547d2ef2-1e6c-60d6-cedc-575060b9d683","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:e2371bec194948cdaeb59d67afae2142@thread.skype"}};
const oddTestingGc = {"id":"1579657462094","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:4a0a7c7b2aa94600b1a560fbaec57b6f@thread.skype"}};
const leGc = {"id":"f:1535a25e-bd4a-8fc2-1ac3-5d675fa738a7","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:d478d779265d48b288ee6e340be5db97@thread.skype"}};
const ngrokGc = {"id":"1580622583704","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:e91df59116e04021bf00e18c7d1a6b47@thread.skype"}}
const hrEmailGc = {"id":"1581022055435","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:52de958d188b49f487ccbe7f7fb69d87@thread.skype"}};
const btaEmailGc = {"id":"1581022145545","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:dc6fd6ce01604b739e37a10746e1b7b1@thread.skype"}};
const payrollHoursGc = {"id":"1581103419303","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:ec18afd2e9334f88a1aa10b60a6296ea@thread.skype"}};

const gcAddress = {
  "7" : {"id":"f:4b1823ed-158f-1b91-0825-9ef3b0de562f","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:25266e5ddc574232ac2fa5e4e1e5b16a@thread.skype"}},
  "13" : {"id":"f:4568aec2-e267-5d34-d7c6-136213363bd0","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:3ddc293235e3464e9905a266beba53fa@thread.skype"}},
  "17" : {"id":"f:bfd48eb1-3cf2-2585-f0ee-1c3f0c4b9cac","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:e1366a08e03c43f2a11c7ce4ed9147e6@thread.skype"}},
  "12" : {"id":"f:fb5f9b4b-8e9a-41d1-44dd-42979961e91e","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:5debd14839654c74b07e28c35a454fe0@thread.skype"}},
  "6" : {"id":"f:ffeaf94e-5139-228b-dd09-913885c80442","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:5a4859253bb141b0b02507041a8cf632@thread.skype"}},
  "15" : {"id":"f:ee8e34f7-8c60-4b29-91f5-d3fff411fa94","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:98f66ad79fd24b9bbfb9bbac3583cb56@thread.skype"}},
  "14" : {"id":"f:8de53794-a10b-4f9e-3e01-9f11ef8822ca","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:057f545a8b9d47fb9432c6187d2ea6df@thread.skype"}},
  "26" : {"id":"f:6152607a-6212-5fbf-4f49-9015a933153b","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:599756eeb1c64efe8f83d5723adc2564@thread.skype"}},
  "22" : {"id":"f:ba71b372-9787-3f5d-d076-f9df743f51ca","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:2a7f5c7b02cf44839a94bd8e3fe5d927@thread.skype"}},
  "8" : {"id":"f:2fb5a4a4-a4d9-670d-27de-006d6e6eee13","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:fe4f2ea949164f3cb613198cef062152@thread.skype"}},
  "23" : {"id":"f:5c10caec-0105-7d66-27af-825f4ef9a80c","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:4e5dd52b11644382a71e595e46c5460e@thread.skype"}},
  "10" : {"id":"f:0d7d0b0f-0422-2aee-44c7-a700aeb57146","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:380c3ffc7ec349a9bb92b6f733812e73@thread.skype"}},
  "3" : {"id":"f:d812d384-ed3f-f564-4bcc-c0b0bfcc0a36","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:c958adf15047492482724cf166ecc77f@thread.skype"}},
  "24" : {"id":"f:71c0667a-a93c-5cc1-b77e-5b17076cf471","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:fe44dd8062b947608ff7dfc782a56759@thread.skype"}},
  "21" : {"id":"1574217240877","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:2a7f5c7b02cf44839a94bd8e3fe5d927@thread.skype"}},
   "27" : {"id":"1579559545513","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:936e2420057849c1a43328381b8ef975@thread.skype"}},
   "49" : {"id":"1581103419303","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:ec18afd2e9334f88a1aa10b60a6296ea@thread.skype"}}
}

const soxGcAddress = {
	"1739" : {"id":"f:f92f4454-7409-10c8-7255-617952185ad6","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:54318b7751ee4b2e97c57f06da3d2f37@thread.skype"}},
	"1060" : {"id":"f:7fc501b8-2864-0b19-bb78-069199cb5578","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:a8d43a64afaf4f018cc45bb6d5250dbc@thread.skype"}},
	"26" : {"id":"f:edb58454-f683-3910-46ff-68db8f8b5fb0","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:4c11a25b7199420fa590de1b56a36f3a@thread.skype"}},
	"1687" : {"id":"f:ef451c70-659a-490c-f6a8-733f9bae2275","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:022c453a02464d99ab9b682e39c959a1@thread.skype"}},
	"33" : {"id":"f:c3f6e82f-466a-e652-a45a-0644ba46cc23","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:e451f1aee2d24fab80396ebb6fdf759b@thread.skype"}},
	"1751" : {"id":"f:1e9f0c65-5413-6f96-1fc6-d945dee2a45b","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:550ec14705ef4d43a3aea406bfeff646@thread.skype"}},
	"1691" : {"id":"f:ac1bdbc7-4417-f895-1d48-6395623124aa","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:7e35db8f147247c391bd2f08688c2407@thread.skype"}},
	"1759" : {"id":"1575916866865","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:167c5647ac384547921593136fc29336@thread.skype"}},
	"1757" : {"id":"1575917106627","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d506a5f0fc749f3b8650d6a22134886@thread.skype"}},
	"1678" : {"id":"1575919218686","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:d6161bbb6ec7472792ce59c9868894d5@thread.skype"}},
	"1756" : {"id":"1577467293412","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:9d82916d3037408b96ffe91afc7b89ee@thread.skype"}},
	"929" : {"id":"1577467864559","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:7131d7b9010b4279a63af1a68af74702@thread.skype"}}
}

app.use(express.json())
app.get('/', (req, res) => res.send('Hello ODD'))

app.post('/api/time-notification', function (req, res) { 
	console.log(req.body);
	try{
	let notif = new OddNotification();
	let link = req.body.link;
	let dateCreated = req.body.dateCreated;
	let empName = req.body.empName;
	let hoursStatus = req.body.hoursStatus;
	let clientRef = req.body.clientRef;
	let taskRef = req.body.taskRef;
	let team = req.body.team;
	let comments = req.body.comments;
	//prod	
	notif.sendMessage(dateCreated + "<br/>" +
					  empName     + "<br/>" +
					  hoursStatus + "<br/>" +
					  clientRef   + "<br/>" +
					  comments     + "<br/>" +
					  link        + "<br/><br/>" 
							
	, payrollHoursGc);
	if (team == 'sox'){
		notif.sendMessage(dateCreated + "<br/>" +
						  empName     + "<br/>" +
						  hoursStatus + "<br/>" +
						  clientRef   + "<br/>" +
						  comments     + "<br/>" +
						  link        + "<br/><br/>" 
								
		, soxTestingTimeNotification);		
	} else if (team == 'bk') {
		notif.sendMessage(dateCreated + "<br/>" +
						  empName     + "<br/>" +
						  hoursStatus + "<br/>" +
						  clientRef   + "<br/>" +
						  comments     + "<br/>" +
						  link        + "<br/><br/>" 
								
		, bkTestingTimeNotification);		
	} 
	
	}catch(error){
		console.log('--time notificaton---');
		console.log(error);
	}
	
	res.send("success");
});

app.post('/api/meeting-post', function (req, res) {

  let notif = new OddNotification();
  let meetingList = req.body.meetings;
  let summary =  Object.entries(meetingList).map(x => {
	  return x[1] + "<br/>" ;
  }).join('');
  notif.sendMessage('------Next Calls----' + "<br/>" + summary, testBotGc);
  
  // prod
  notif.sendMessage('------Next Calls----' + "<br/>" + summary, meetingsMon);
  
  console.log(summary);
  res.send("success");
});

app.post('/api/sox-summary', function (req, res) {

  let notif = new OddNotification();
  let summaryTotal = req.body.totalCount;
  let summaryList = req.body.summaryList;
  let summary =  Object.entries(summaryList).map(x => {
	  return x[0]+ " : " + x[1] + "<br/>" ;
  }).join('');
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Assigned To: " + summaryTotal + "<br/><br/>" + summary, testBotGc);
  
  //prod
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Assigned To: " + summaryTotal + "<br/><br/>" + summary, soxMonGC);
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Assigned To: " + summaryTotal + "<br/><br/>" + summary, soxPaTeam);
   
  console.log(summary);
  res.send("success");
});

app.post('/api/bk-summary', function (req, res) {

  let notif = new OddNotification();
  let summaryTotal = req.body.totalCount;
  let summaryList = req.body.summaryList;
  let summary =  Object.entries(summaryList).map(x => {
	  return x[0]+ " : " + x[1] + "<br/>" ;
  }).join('');
  // prod
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Waiting On: " + summaryTotal + "<br/><br/>" + summary, testBotGc);
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Waiting On: " + summaryTotal + "<br/><br/>" + summary, bkMonGC);
  
  console.log(summary);
  res.send("success");
});


app.post('/api/sox-status', function (req, res) {
  let soxTestGc = {"id":"1574219371980","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:695f620a8b0044b68b4ea42e1eb79395@thread.skype"}}; 
  let notif = new OddNotification();
  let newStatus = req.body.new;
  let inProcess = req.body.inProcess;
  let pendingReview = req.body.pendingReview;
  let waitingOnClient = req.body.waitingOnClient;
  let newOpenOnHold = req.body.newOpenOnHold;
  let closed = req.body.closed;
  
  // prod 
  notif.sendMessage("New: " + newStatus + "<br/>" + "In Process: " + inProcess + "<br/>" + "Pending Review: " + pendingReview + "<br/>" + "Waiting On Client: " + waitingOnClient + "<br/>" + "New Open On Hold: " + newOpenOnHold + "<br/>" + "Closed: " + closed , soxMonGC);
  notif.sendMessage("New: " + newStatus + "<br/>" + "In Process: " + inProcess + "<br/>" + "Pending Review: " + pendingReview + "<br/>" + "Waiting On Client: " + waitingOnClient + "<br/>" + "New Open On Hold: " + newOpenOnHold + "<br/>" + "Closed: " + closed , soxPaTeam); 
  console.log(req.body);
  res.send("success");
});

app.post('/api/bk-status', function (req, res) {
  let soxTestGc = {"id":"1574219371980","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:695f620a8b0044b68b4ea42e1eb79395@thread.skype"}}; 
  let notif = new OddNotification();
  let newStatus = req.body.new;
  let inProcess = req.body.inProcess;
  let pendingReview = req.body.pendingReview;
  let waitingOnClient = req.body.waitingOnClient;
  let updated = req.body.updated;
  let onHold = req.body.onHold;
  let approvedClient = req.body.approvedClient;
  let closed = req.body.closed;
  let monitoredYes = req.body.monitoredYes;
  let monitoredNo = req.body.monitoredNo;
  
  
  
   //prod 
   notif.sendMessage("------------------------------" + "<br/>" +
					"Monitored(Open Items)" + "<br/>" +
					"Yes: " + monitoredYes + "<br/>" + 
					"No: " + monitoredNo + "<br/>" +
					"------------------------------"+ "<br/>" +
					"New: " + newStatus + "<br/>" + 
					"In Process: " + inProcess + "<br/>" + 
					"Pending Review: " + pendingReview + "<br/>" + 
					"Waiting On Client: " + waitingOnClient + "<br/>" + 
					"Updated: " + updated + "<br/>" + 
					"On Hold: " + onHold + "<br/>" + 
					"Approved Client: " + approvedClient + "<br/>" + 
					"Closed: " + closed , 
					bkMonGC);
  console.log(req.body);
  res.send("success");
});

app.post('/api/odd-post', function (req, res) {
  console.log("=====bk======");
  let notif = new OddNotification();
  let oaiTitle = req.body.oaiTitle
  let clientName = req.body.clientName
  let clientId = req.body.clientId;
  let link = req.body.link;
  let requestFrom = req.body.requestFrom;
  let waitingOn = req.body.waitingOn !== null ? req.body.waitingOn : '';
  let assignedTo = req.body.assignedTo !== null ? req.body.assignedTo : '';

  let bkGC = {"id":"f:66060e9f-2b30-28ce-daee-4f10ac6f4fe2","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:5ad90cd5d6ee444b8a9cd45ed01d30e2@thread.skype"}};
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
  let BKEmailAutomationGC = { id: '1574102262523', channelId: 'skype', serviceUrl: 'https://smba.trafficmanager.net/apis/', conversation:{ isGroup: true,id: '19:c11f3cdea3e24ce9939bb444c50a55ae@thread.skype' }};	
	
  	// prod
    notif.sendMessage( oaiTitle + "<br/>"+ 
					   link + "<br/><br/>" + 
					   requestFrom + "<br/><br/>" +
					   "Waiting On: "+ waitingOn + "<br/>"+ 
					   "Assigned To: "+ assignedTo + "<br/>"+
					   "Please action and status accordingly. Thank you."
					   , gcAddress[clientId]);
	notif.sendMessage( oaiTitle + "<br/>"+ 
					   link + "<br/><br/>" + 
					   requestFrom + "<br/><br/>" +
					   "Waiting On: "+ waitingOn + "<br/>"+ 
					   "Assigned To: "+ assignedTo + "<br/>"+
					   "Please action and status accordingly. Thank you."
					   , BKEmailAutomationGC); 

	

  console.log(req.body);
  res.send("success");
});

app.post('/api/sox-post', function (req, res) {
  console.log("====sox=====");
  let soxTestGc = {"id":"1574219371980","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:695f620a8b0044b68b4ea42e1eb79395@thread.skype"}}; 
  let notif = new OddNotification();
  let oaiTitle = req.body.oaiTitle  
  let clientId = req.body.clientId;
  let waitingOn = req.body.waitingOn !== null ? req.body.waitingOn : '';
  let assignedTo = req.body.assignedTo !== null ? req.body.assignedTo : '';
  let link = req.body.link;
  let requestInfo = req.body.requestInfo;
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
  try{

	// prod  
    notif.sendMessage(oaiTitle + "<br/>"+ 
					  link + "<br/><br/>" + 
					  requestInfo + "<br/><br/>" + 
					  "Waiting On: "+ 
					  waitingOn + "<br/>"+ 
					  "Assigned To: "+ assignedTo + "<br/>"+
					  "Please action and status accordingly. Thank you."
					  , soxTestGc);
    notif.sendMessage(oaiTitle + "<br/>"+ 
					  link + "<br/><br/>" + 
					  requestInfo + "<br/><br/>" + 
					  "Waiting On: "+ 
					  waitingOn + "<br/>"+ 
					  "Assigned To: "+ assignedTo + "<br/>"+
					  "Please action and status accordingly. Thank you."
					  , soxGcAddress[clientId]); 
					  
					  
    console.log(req.body);
    console.log('skypeadd', soxGcAddress[clientId]);
  }catch(error){
    console.log(error);
  }
  
  res.send("success");
});

app.post('/api/ns-post', function (req, res) {
  console.log("=======ns=======");
  
  let notif = new OddNotification();
  let oaiTitle = req.body.oaiTitle  
  let clientId = req.body.clientId;
  let assignedTo = req.body.assignedTo !== null ? req.body.assignedTo : '';
  let link = req.body.link;
  let reviewer = req.body.reviewer;
  let status = req.body.status;
  let requestInfo = req.body.requestInfo;
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
  + "<br/>" //011620
  
  
  //prod 
  notif.sendMessage(oaiTitle + "<br/>"+ link +  "<br/><br/>" + requestInfo +  "<br/><br/>" + "Assigned To: " + assignedTo +"<br/>" + "Reviewer: " + reviewer + "<br/>" + "Status: " + status + "<br/>" + "Please action and status accordingly. Thank you.", ns);
  
 
  console.log(req.body);
  res.send("success");
});



app.post('/api/hd-post', function (req, res) {
  console.log("=======hd=======");
  let a2bops = {"id":"f:f80295d3-9807-20ba-650d-0c0b3f4e868f","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:ca5256cc7e804ea0836f2c539ba8a85c@thread.skype"}};
  let notif = new OddNotification();
  let oaiTitle = req.body.oaiTitle  
  let clientId = req.body.clientId;
  let requestInfo = req.body.requestInfo;
  let link = req.body.link;
  let waitingOn = typeof req.body.waitingOn !== undefined ? req.body.waitingOn : '';
  let assignedTo = typeof req.body.assignedTo !== undefined ? req.body.assignedTo : '';
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
 
  //prod
   notif.sendMessage(oaiTitle + "<br/>"+ link + "<br/><br/>" + requestInfo +  "<br/><br/>" + "Waiting On: "+ waitingOn + "<br/>"+ "Assigned To: "+ assignedTo + "<br/>"+"Please action and status accordingly. Thank you." + "<br/>" + "<br/>" + "<br/>", hd); 
	
  console.log(req.body);
  res.send("success");
});

app.post('/api/dl-post', function (req, res) {
  console.log("=======dl=======");
  let notif = new OddNotification();
  let requestInfo = req.body.requestInfo;
  let oaiTitle = req.body.oaiTitle  
  let clientId = req.body.clientId;
  let link = req.body.link;
  let waitingOn = req.body.waitingOn !== null ? req.body.waitingOn : '';
  let assignedTo = req.body.assignedTo !== null ? req.body.assignedTo : '';
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
  
  //prod
  
   notif.sendMessage(oaiTitle + "<br/>"+ link + "<br/><br/>" + requestInfo +  "<br/><br/>" + "Waiting On: "+ waitingOn + "<br/>"+ "Assigned To: "+ assignedTo + "<br/>"+"Please action and status accordingly. Thank you." + "<br/>" + "<br/>" + "<br/>", dl); 
  notif.sendMessage(oaiTitle + "<br/>"+ link + "<br/><br/>" + requestInfo +  "<br/><br/>" + "Waiting On: "+ waitingOn + "<br/>"+ "Assigned To: "+ assignedTo + "<br/>"+"Please action and status accordingly. Thank you." + "<br/>" + "<br/>" + "<br/>", dlAdmin); 
  
  console.log(req.body);
  res.send("success");
});

app.post('/api/dl-summary', function (req, res) {

  let notif = new OddNotification();
  let summaryTotal = req.body.totalCount;
  let summaryList = req.body.summaryList;
  let summary =  Object.entries(summaryList).map(x => {
	  return x[0]+ " : " + x[1] + "<br/>" ;
  }).join('');
  
  //prod
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Waiting On: " + summaryTotal + "<br/><br/>" + summary, dl);
  console.log(summary);
  res.send("success");
});

app.post('/api/help-summary', function (req, res) {

  let notif = new OddNotification();
  let summaryTotal = req.body.totalCount;
  let summaryList = req.body.summaryList;
  let summary =  Object.entries(summaryList).map(x => {
	  return x[0]+ " : " + x[1] + "<br/>" ;
  }).join('');
 
  // prod
  notif.sendMessage("Summary of Assignments" + "<br/><br/>" + "Waiting On: " + summaryTotal + "<br/><br/>" + summary, hd);
  console.log(summary);
  res.send("success");
});

app.post('/api/sox-status', function (req, res) {
  console.log("=======sox status=======");
  let soxTestGc = {"id":"1574219371980","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:695f620a8b0044b68b4ea42e1eb79395@thread.skype"}}; 
  let notif = new OddNotification();
  let statusNew = req.body.new;  
  let statusInprocess = req.body.inProcess;
  let statusPendingReview = req.body.pendingReview;
  let statuswaitingOnClient = req.body.waitingOnClient;
  let statusnewOpenOnHold = req.body.newOpenOnHold;
  let statusClosed = req.body.closed;
  let message = "New: "+ statusNew + "<br/>"+ "In Process: " + statusInprocess + "<br/>" + "Pending Review: " + statusPendingReview + "<br/>"+ "Waiting on Client: " + statuswaitingOnClient + "<br/>" + "New Open On Hold: " + statusnewOpenOnHold + "<br/>"+ "Closed: " + statusClosed;
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
  
  //prod
  notif.sendMessage( message, soxMonGC);
  console.log(req.body);
  res.send("success");
});

app.post('/api/dl-status', function (req, res) {
  let notif = new OddNotification();
  let newStatus = req.body.new;
  let inProcess = req.body.inProcess;
  let pendingReview = req.body.pendingReview;
  let waitingOnClient = req.body.waitingOnClient;
  let onHold = req.body.onHold;
  let approvedClient = req.body.approvedClient;
  let closed = req.body.closed;
					
					
  // prod
    notif.sendMessage("New: " + newStatus + "<br/>" + 
					"In Process: " + inProcess + "<br/>" + 
					"Pending Review: " + pendingReview + "<br/>" + 
					"Waiting On Client: " + waitingOnClient + "<br/>" + 
					"On Hold: " + onHold + "<br/>" +
					"Approved Client: " + approvedClient + "<br/>" + 
					"Closed: " + closed , dl);
					
  console.log(req.body);
  res.send("success");
});


app.post('/api/help-status', function (req, res) {
  let notif = new OddNotification();
  let newStatus = req.body.new;
  let inProcess = req.body.inProcess;
  let moveToProjects = req.body.moveToProjects;
  let waitingForReview = req.body.waitingForReview;
  let onHold = req.body.onHold;
  let closed = req.body.closed;
  // prod 				
   notif.sendMessage("New: " + newStatus + "<br/>" + 
					"In Process: " + inProcess + "<br/>" + 
					"Move To Projects: " + moveToProjects + "<br/>" + 
					"Waiting For Review: " + waitingForReview + "<br/>" + 
					"On Hold: " + onHold + "<br/>" +
					"Closed: " + closed , hd);
					
  console.log(req.body);
  res.send("success");
});
//011320
app.post('/api/ns-status', function (req, res) {
  let notif = new OddNotification();
  let newStatus = req.body.new;
  let inProcess = req.body.inProcess;
  let pendingReview = req.body.pendingReview;
  let waitingOnClient = req.body.waitingOnClient;
  let onHold = req.body.onHold;
  let closed = req.body.closed;

  //prod			
   notif.sendMessage("New: " + newStatus + "<br/>" + 
					"In Process: " + inProcess + "<br/>" + 
					"Pending Review: " + pendingReview + "<br/>" + 
					"Waiting On Client: " + waitingOnClient + "<br/>" + 
					"On Hold: " + onHold + "<br/>" +
					"Closed: " + closed , ns);
  console.log(req.body);
  res.send("success");
});


// new format
app.post('/api/bta-post', function (req, res) {
  let notifProject = req.body.notif;
  console.log("===="+notifProject+"=====");
  let notif = new OddNotification();
  let oaiTitle = req.body.oaiTitle  
  let clientId = req.body.clientId;
  let waitingOn = req.body.waitingOn !== null ? req.body.waitingOn : '';
  let assignedTo = req.body.assignedTo !== null ? req.body.assignedTo : '';
  let link = req.body.link;
  let requestInfo = req.body.requestInfo;
  let testBotGc = {"id":"1573375103672","channelId":"skype","serviceUrl":"https://smba.trafficmanager.net/apis/","conversation":{"isGroup":true,"id":"19:6d67dd0b9af6438896e21f1a6c64addd@thread.skype"}};
  try{

	// prod  
    notif.sendMessage("====="+ notifProject.toUpperCase() + "Email ======" + "<br/>"+ 
					  oaiTitle + "<br/>"+ 
					  link + "<br/><br/>" + 
					  requestInfo + "<br/><br/>" + 
					  "Waiting On: "+ 
					  waitingOn + "<br/>"+ 
					  "Assigned To: "+ assignedTo + "<br/>"+
					  "Please action and status accordingly. Thank you."
					  , notifProject === "bta" ? btaEmailGc : hrEmailGc);
					  
    console.log(req.body);
    console.log('skypeadd', soxGcAddress[clientId]);
  }catch(error){
    console.log(error);
  }
  
  res.send("success");
});

app.post('/api/skype-messages', function (req, res) {
		try {
			address = {
				id : req.body.id,
				channelId : req.body.channelId,
				serviceUrl : req.body.serviceUrl,
				conversation : req.body.conversation
			}
			console.log(JSON.stringify(address));
			res.send("success");
		} catch ( e ) {
			console.log("Error:", e);
		}
});

app.post('/api/skype-ordering', function (req, res) {
  console.log(gcAddress[9]);
  res.send("success");
});

app.listen(port, () => console.log(`app listening on port ${port}!`))