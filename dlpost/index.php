<?php
//Enable error display
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
set_time_limit(0);

$_SERVER['DOCUMENT_ROOT'] = "C:/xampp/htdocs";
include_once $_SERVER['DOCUMENT_ROOT']."/hook/classes/cls-constant.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-4.3.0/PodioAPI.php";
include_once "crud-oop.php";

Podio::$debug = true;
$file = "webhook_a2b_time.log";

Podio::setup(Cons::CLIENT_ID, Cons::CLIENT_SECRET);

if($_POST){
	switch ($_POST['type']) {
			case 'hook.verify':
				PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
				break;
			case 'item.update':
				
				 $json_str = file_get_contents('php://input');
				 
				 file_put_contents('logs/update/dlpostupdate'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				 $itemId = $_POST['item_id'];
				 dlPost($itemId);
				break;
			case 'item.create':
			    $itemId = $_POST['item_id'];
			    $json_str = file_get_contents('php://input');
				file_put_contents('logs/create/dlpost'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				dlPost($itemId);
				break;
	}
} 

function dlPost($itemId){
	
	$app_id = "20838497";
	$app_token = "55b70aa20f214433922c50ef1ce1d6a3";
	Podio::authenticate_with_app($app_id, $app_token);
	
	try{
		//sleep(5);
		$items = PodioItem::get( $itemId );
		$requestInfo =$items->fields['project-description']->values;
		$assignedTo = $items->fields['contact']->values[0]->name;
		$waitingOn = $items->fields['waiting-on']->values[0]->name;
		$data = array();
		$data = array(
			/* "clientId"  => isset($items->fields['clients']->values[0]->app_item_id) ? $items->fields['clients']->values[0]->app_item_id : null , */
			"oaiTitle" => $items->title,
			//"cliendOai" => $items->fields['client-oai-3']->values[0]['text'],
			"itemId" => $itemId,
			"link" => $items->link,
			"requestInfo" => mb_strimwidth(strip_tags($requestInfo), 0, 300),
			"assignedTo" => $assignedTo,
			"waitingOn" => $waitingOn
		);
		$oaiTitle = $items->title;
		$data = json_encode($data);
		//echo $data;
	//	file_put_contents('logs/create/json/dlpost'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
		if(!checkDuplicate($oaiTitle,$itemId)){
			if($oaiTitle !== NULL){
				insertItemId($itemId,$oaiTitle);
				sendToOdd($data);
			}
		} else{
			if(checkIfAllow($oaiTitle, $itemId)){
				sendToOdd($data);
			}
		} 
		//sendToOdd($data);
 	   /*  if($items->title !== NULL ){
			if(!checkIdExistAndTrue($itemId)){
				insertItemId($itemId);
				sendToOdd($data);
			}
		}  */
		
	} catch (Exception $e) {
		file_put_contents('error.log', $e->getMessage(), FILE_APPEND | LOCK_EX);
	}
}
//dlPost($itemId);
//dlPost(1262474254);
function sendToOdd($data){
	$postData = $data;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_PORT => "1337",
	CURLOPT_URL => "http://localhost:1337/api/dl-post",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 120,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $postData,
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json",
		"postman-token: 262c4c0c-2b80-f794-f9a0-13f0d8a9520e"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	file_put_contents('curlerror.log', $err, FILE_APPEND | LOCK_EX);
	echo "cURL Error #:" . $err;
	} else {
	echo $response;
	}
}
/* if(checkOAI($itemId)){
	
} */

function checkIdExistAndTrue($id){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','itemId',NULL,'itemId='. $id,'itemId DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	if(count($res) > 0){
		echo count($res);
		return true;
	} else {
		return false;
	}
	
}

function checkDuplicate($oaiTitle,$itemId){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','oaiTitle,dateCreated,id',NULL,"oaiTitle='".$oaiTitle."'",'dateCreated DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	//file_put_contents('logs/cd/'.$itemId.date('Ymdhis').'.log',$res, FILE_APPEND | LOCK_EX);
	try{
		if(count($res) > 0){
			return true;
		} else {
			return false;
		}		
	} catch(Exception $e) {
		echo $e->getMessage();
	}
	
}

function update($id){
	$db = new Database();
	$db->connect();
	$db->update('tbl_checker',array('dateCreated'=> date('Y-m-d h:i:s')),'id='.$id); // Table name, column names and values, WHERE conditions
	$res = $db->getResult();
}

function insertItemId($id,$oaiTitle){
	$db = new Database();
	$db->connect();
	$db->insert('tbl_checker',array('itemId'=>$id,'triggeredNotif' => true,'workspace' => 'dl','oaiTitle'=> $oaiTitle));
}

function checkIfAllow($oaiTitle,$itemId){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','oaiTitle,dateCreated,id',NULL,"oaiTitle='".$oaiTitle."'",'dateCreated DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	//file_put_contents('logs/cia/'.$itemId.date('Ymdhis').'.log',$res, FILE_APPEND | LOCK_EX);
	$tempId = $res[0]['id'];
	$dateCreated = strtotime($res[0]['dateCreated']);
	$dateForToday = strtotime("now");
	$thirtyMinsAgo = strtotime("+30 minutes",$dateCreated);
	if($dateForToday > $thirtyMinsAgo){
		update($tempId);
		return true;
	} else  {
		return false;
	}
}

?>