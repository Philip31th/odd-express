<?php
//Enable error display
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
set_time_limit(0);

$_SERVER['DOCUMENT_ROOT'] = "C:/xampp/htdocs";
include_once $_SERVER['DOCUMENT_ROOT']."/hook/classes/cls-constant.php";
// require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-4.3.0/PodioAPI.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-master/PodioAPI.php";
include_once "crud-oop.php";

jobFunction();

function jobFunction(){
	$listOfDeletedData = Array();
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','oaiTitle,dateCreated,id',NULL,NULL,'dateCreated DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	$dateForToday = strtotime("now");
    if(count($res) > 0){
		foreach($res as $dataResponse){
			$dataResponseId = $dataResponse['id'];
			$dateCreated = strtotime($dataResponse['dateCreated']);
			$thirtyMinsAgo = strtotime("+30 minutes",$dateCreated);
			if($dateForToday > $thirtyMinsAgo){
				$listOfDeletedData[]= array(
					'id' 		  => $dataResponseId,
					'dateCreated' => $dateCreated,
					'dateForToday'=> $dateForToday,
					'expiryDate'  => $thirtyMinsAgo
				);
				deleteData($dataResponseId);
			} 
		}		
	}
	$resultData = json_encode($listOfDeletedData);
	if(count($listOfDeletedData) > 0){
	file_put_contents(dirname(__FILE__).'/logs/delete/sox'.date('Ymdhis').'.log',$resultData, FILE_APPEND | LOCK_EX);
	}
	return $resultData;
}

function deleteData($id){
	$db = new Database();
	$db->connect();
	$db->delete('tbl_checker','id='.$id);  // Table name, WHERE conditions
	$res = $db->getResult();  
}

?>