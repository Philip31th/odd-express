<?php
//Enable error display
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
set_time_limit(0);

$_SERVER['DOCUMENT_ROOT'] = "C:/xampp/htdocs";
include_once $_SERVER['DOCUMENT_ROOT']."/hook/classes/cls-constant.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-4.3.0/PodioAPI.php";
include_once "crud-oop.php";

Podio::$debug = true;
$file = "webhook_a2b_time.log";

Podio::setup(Cons::CLIENT_ID, Cons::CLIENT_SECRET);

if($_POST){
	switch ($_POST['type']) {
			case 'hook.verify':
				PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
				break;
			case 'item.update':
				
				 $json_str = file_get_contents('php://input');
				 
				 file_put_contents('logs/update/soxpostupdate'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				 $itemId = $_POST['item_id'];
				// $revisionId = 1;
				soxPost($itemId);
				// $revision = PodioItemRevision::get( $itemId, $revisionId );
				
				// file_put_contents('revision'.date('Ymdhis').'.log',$revision, FILE_APPEND | LOCK_EX);
				break;
			case 'item.create':
			    $itemId = $_POST['item_id'];
			    $json_str = file_get_contents('php://input');
				file_put_contents('logs/create/soxpost'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				soxPost($itemId);
				break;
	}
}


// sox  
//soxPost(1283718446);
//hd
// 1286172388, 1286137589
//soxPost(1325045194);

function soxPost($itemId){
	// hd
	//$app_id = "13004064";
	//$app_token = "f52668da11d146b6b91b57568e6afa98";
	// sox
	$app_id = "18420633";
	$app_token = "575e96f075f1456dad25ee7ecda0cc72"; 
	Podio::authenticate_with_app($app_id, $app_token);
	
	try{
		// Buffer all upcoming output...
		ob_start();

		// Send your response.
		//echo "Request Sent";

		// Get the size of the output.
		$size = ob_get_length();

		// Disable compression (in case content length is compressed).
		header("Content-Encoding: none");

		// Set the content length of the response.
		header("Content-Length: {$size}");

		// Close the connection.
		header("Connection: close");

		// Flush all output.
		ob_end_flush();
		ob_flush();
		flush();
		sleep(60);
		$items = PodioItem::get( $itemId );
		
		$data = array();
		$requestInfo = $items->fields['text']->values;
		$assignedTo = $items->fields['current-owner']->values[0]->name;
		$waitingOn = $items->fields['waiting-on']->values[0]->name;
		$clientId = $items->fields['clients']->values[0]->app_item_id;
		$oaiTitle = $items->title;
		$data = array(
			"clientId"  => $clientId,
			"oaiTitle" => $oaiTitle,
			"requestInfo" => mb_strimwidth(strip_tags($requestInfo), 0, 300),
			"itemId" => $itemId,
			"link" => $items->link,
			"assignedTo" => isset($assignedTo) ? $assignedTo : '',
			"waitingOn" => isset($waitingOn) ? $waitingOn : ''
		);
		//$clientName = $items->fields['client-2']->values[0]['text'];
		//$clientId = $items->fields['client-2']->values[0]['id'];
		$data = json_encode($data);
		echo $data;
		$status = $items->fields['status-ref']->values;
 		file_put_contents('logs/create/json/soxpost'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
		if(!checkDuplicate($oaiTitle)){
			if(isset($clientId) && $clientId !== NULL && $status !== 'Closed'){
				insertItemId($itemId,$oaiTitle);
				sendToOdd($data);
			}
		}  
		/* else {
			if(checkIfAllow($oaiTitle)){
				if($waitingOn != '' && $assignedTo != ''){
					sendToOdd($data);
				}
			}
		}  */
	} catch(Exception $e) {
		file_put_contents('error.log', $e->getMessage(), FILE_APPEND | LOCK_EX);
	}
}


function checkIfAllow($oaiTitle){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','oaiTitle,dateCreated,id',NULL,"oaiTitle='".$oaiTitle."'",'dateCreated DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	//file_put_contents('logs/cia/cia'.date('Ymdhis').'.log',json_encode($res), FILE_APPEND | LOCK_EX);
	$tempId = $res[0]['id'];
	$dateCreated = strtotime($res[0]['dateCreated']);
	$dateForToday = strtotime("now");
	$thirtyMinsAgo = strtotime("+30 minutes",$dateCreated);
	if($dateForToday > $thirtyMinsAgo){
		update($tempId);
		return true;
	} else  {
		return false;
	}
}


function sendToOdd($data){
	$postData = $data;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_PORT => "1337",
	CURLOPT_URL => "http://localhost:1337/api/sox-post",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 120,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $postData,
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json",
		"postman-token: 262c4c0c-2b80-f794-f9a0-13f0d8a9520e"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	echo "cURL Error #:" . $err;
	} else {
	echo $response;
	}
}
/* if(checkOAI($itemId)){
	
} */

function checkIdExistAndTrue($id){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','itemId',NULL,'itemId='. $id,'itemId DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	if(count($res) > 0){
		//echo count($res);
		return true;
	} else {
		return false;
		//echo 'failed';
	}
	
}

function insertItemId($id,$oaiTitle){
	$db = new Database();
	$db->connect();
	$db->insert('tbl_checker',array('itemId'=>$id,'triggeredNotif' => true,'workspace' => 'sox','oaiTitle'=> $oaiTitle));
}


function checkDuplicate($oaiTitle){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','oaiTitle,dateCreated,id',NULL,"oaiTitle='".$oaiTitle."'",'dateCreated DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	file_put_contents('logs/cd/cd'.date('Ymdhis').'.log',count($res), FILE_APPEND | LOCK_EX);
	try{
		if(count($res) > 0){
			return true;
		} else {
			return false;
		}		
	} catch(Exception $e) {
		file_put_contents('error.log',$e, FILE_APPEND | LOCK_EX);
		echo $e->getMessage();
	}
	
}  

function update($id){
	$db = new Database();
	$db->connect();
	$db->update('tbl_checker',array('dateCreated'=> date('Y-m-d h:i:s')),'id='.$id); // Table name, column names and values, WHERE conditions
	$res = $db->getResult();
}

?>