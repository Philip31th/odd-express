<?php
//Enable error display
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
set_time_limit(0);

$_SERVER['DOCUMENT_ROOT'] = "C:/xampp/htdocs";
include_once $_SERVER['DOCUMENT_ROOT']."/hook/classes/cls-constant.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-4.3.0/PodioAPI.php";
include_once "crud-oop.php";

Podio::$debug = true;
$file = "webhook_a2b_time.log";

Podio::setup(Cons::CLIENT_ID, Cons::CLIENT_SECRET);

if($_POST){
	switch ($_POST['type']) {
			case 'hook.verify':
				PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
				break;
			case 'item.update':
				
				 $json_str = file_get_contents('php://input');
				 
				 file_put_contents('logs/update/hdpostupdate'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				 $itemId = $_POST['item_id'];
				 hdUpdatePost($itemId);
				// $revisionId = 1;
				// bkPost($itemId);
				// $revision = PodioItemRevision::get( $itemId, $revisionId );
				
				// file_put_contents('revision'.date('Ymdhis').'.log',$revision, FILE_APPEND | LOCK_EX);
				break;
			case 'item.create':
			    $itemId = $_POST['item_id'];
			    $json_str = file_get_contents('php://input');
				file_put_contents('logs/create/hdpost'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				hdPost($itemId);
				break;
	}
} 

function hdPost($itemId){
	
	$app_id = "13004064";
	$app_token = "f52668da11d146b6b91b57568e6afa98";
	Podio::authenticate_with_app($app_id, $app_token);
	
	try{
		sleep(5);
		$items = PodioItem::get( $itemId );
		$oaiStatus = $items->fields['status']->values[0]['text'];
		$assignedTo = $items->fields['assigned-to']->values[0]->name;
		$waitingOn = $items->fields['waiting-on']->values[0]->name;
		$data = array();
		$data = array(
			//"clientName" => $items->fields['client-2']->values[0]['text'],
			/* "clientId"  => isset($items->fields['clients']->values[0]->app_item_id) ? $items->fields['clients']->values[0]->app_item_id : null , */
			"oaiTitle" => $items->title,
			//"cliendOai" => $items->fields['client-oai-3']->values[0]['text'],
			"itemId" => $itemId,
			"link" => $items->link,
			"assignedTo" => $assignedTo,
			"waitingOn" => $waitingOn
		);
		$data = json_encode($data);
		file_put_contents('logs/create/json/hdpost'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
 		if($items->title !== NULL && $oaiStatus !== "closed"){
			/* if(!checkIdExistAndTrue($itemId)){
				insertItemId($itemId);
				sendToOdd($data);
			} */  
			sendToOdd($data);
	
		}
	} catch (Exception $e) {
		file_put_contents('error.log', $e->getMessage(), FILE_APPEND | LOCK_EX);
	}
}

function hdUpdatePost($itemId){
	
	$app_id = "13004064";
	$app_token = "f52668da11d146b6b91b57568e6afa98";
	Podio::authenticate_with_app($app_id, $app_token);
	
	try{
		sleep(10);
		$items = PodioItem::get( $itemId );
		$oaiStatus = $items->fields['status']->values[0]['text'];
		$assignedTo = isset($items->fields['assigned-to']->values[0]->name) ? $items->fields['assigned-to']->values[0]->name : '';
		$waitingOn = isset($items->fields['waiting-on']->values[0]->name) ? $items->fields['waiting-on']->values[0]->name : '';
		$data = array();
		$data = array(
			//"clientName" => $items->fields['client-2']->values[0]['text'],
			/* "clientId"  => isset($items->fields['clients']->values[0]->app_item_id) ? $items->fields['clients']->values[0]->app_item_id : null , */
			"oaiTitle" => $items->title,
			//"cliendOai" => $items->fields['client-oai-3']->values[0]['text'],
			"itemId" => $itemId,
			"link" => $items->link,
			"assignedTo" => $assignedTo,
			"waitingOn" => $waitingOn
		);
		$data = json_encode($data);
		file_put_contents('logs/create/json/hdpost'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
 		if($items->title !== NULL && $oaiStatus !== "closed" && $waitingOn !== NULL && $assignedTo !== NULL){
			if(!checkIdExistAndTrue($itemId)){
				insertItemId($itemId);
				sendToOdd($data);
			}  
	
		}
	} catch (Exception $e) {
		file_put_contents('error.log', $e->getMessage(), FILE_APPEND | LOCK_EX);
	}
}
//hdPost($itemId);
//hdPost(1280828762);
function sendToOdd($data){
	$postData = $data;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_PORT => "3000",
	CURLOPT_URL => "http://localhost:3000/api/hd-post",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 120,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $postData,
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json",
		"postman-token: 262c4c0c-2b80-f794-f9a0-13f0d8a9520e"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	file_put_contents('curlerror.log', $err, FILE_APPEND | LOCK_EX);
	echo "cURL Error #:" . $err;
	} else {
	echo $response;
	}
}

/* if(checkOAI($itemId)){
	
} */

function checkIdExistAndTrue($id){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','itemId',NULL,'itemId='. $id,'itemId DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	if(count($res) > 0){
		echo count($res);
		return true;
	} else {
		return false;
	}
	
}

function insertItemId($id){
	$db = new Database();
	$db->connect();
	$db->insert('tbl_checker',array('itemId'=>$id,'triggeredNotif' => true,'workspace' => 'hd'));
}

?>