<?php
//Enable error display
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
set_time_limit(0);

$_SERVER['DOCUMENT_ROOT'] = "C:/xampp/htdocs";
include_once $_SERVER['DOCUMENT_ROOT']."/hook/classes/cls-constant.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-4.3.0/PodioAPI.php";
include_once "crud-oop.php";

Podio::$debug = true;
$file = "err";

Podio::setup(Cons::CLIENT_ID, Cons::CLIENT_SECRET);

if($_POST){
	switch ($_POST['type']) {
			case 'hook.verify':
				PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
				break;

			case 'item.create':
				$json_str = file_get_contents('php://input');
				 
				//file_put_contents('post'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				$itemId = $_POST['item_id'];
			   // $revisionId = 1;
			   bkPost($itemId);
			   // $revision = PodioItemRevision::get( $itemId, $revisionId );
			   
			   // file_put_contents('revision'.date('Ymdhis').'.log',$revision, FILE_APPEND | LOCK_EX);
			   break;	
			case 'item.update':
				
				 $json_str = file_get_contents('php://input');
				 
				 //file_put_contents('post'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				 $itemId = $_POST['item_id'];
				// $revisionId = 1;
				bkPost($itemId);
				// $revision = PodioItemRevision::get( $itemId, $revisionId );
				
				// file_put_contents('revision'.date('Ymdhis').'.log',$revision, FILE_APPEND | LOCK_EX);
				break;
	}
} 

function bkPost($itemId){
	
	$app_id = "19047901";
	$app_token = "0c6b41f9301e4033931481208d0477d7";
	Podio::authenticate_with_app($app_id, $app_token);
	
	try{
		$items = PodioItem::get( $itemId );
		$assignedTo = @$items->fields['assigned-to-ref']->values[0]->title;
		$waitingOn = @$items->fields['waiting-on-ref']->values[0]->title;
		$clientName = $items->fields['client-2']->values[0]['text'];
		$clientId = $items->fields['client-2']->values[0]['id'];
		$oaiTitle = $items->fields['project-title']->values;
		$emailRequest = strip_tags($items->fields['project-description']->values);
		$end = strpos($emailRequest,"Sent");
		$data = array();
		$data = array(
			"clientName" => $items->fields['client-2']->values[0]['text'],
			"clientId"  => $items->fields['client-2']->values[0]['id'],
			"oaiTitle" => $items->fields['project-title']->values,
			"clientOai" => @$items->fields['client-oai-3']->values[0]['text'],
			"itemId" => $itemId,
			"waitingOn" => $waitingOn,
			"assignedTo" => $assignedTo,
			"link" => $items->link,
			//"requestFrom" => substr($emailRequest,0,$end),
			"requestFrom" => mb_strimwidth(strip_tags($emailRequest), 0, 300),
		);
		$data = json_encode($data);
 		 if(@$items->fields['client-2']->values[0]['text'] !== NULL && @$items->fields['client-2']->values[0]['text'] !== "" && @$items->fields['client-oai-3']->values[0]['text'] == 'Yes' && $assignedTo !== NULL && $waitingOn !== NULL && $oaiTitle !== NULL){ 
			if(!checkIdExistAndTrue($itemId)){
				insertItemId($itemId);
				sendToOdd($data);
				file_put_contents('data'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
			}
		}  
		
		
		file_put_contents('post'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
	} catch(Exception $e) {
		file_put_contents('error.log', $e->getMessage(), FILE_APPEND | LOCK_EX);
	}
}

//bkPost(1323993578);
function sendToOdd($data){
	$postData = $data;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_PORT => "1337",
	CURLOPT_URL => "http://localhost:1337/api/odd-post",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 120,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $postData,
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json",
		"postman-token: 262c4c0c-2b80-f794-f9a0-13f0d8a9520e"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	echo "cURL Error #:" . $err;
	file_put_contents('curlerror.log', $err, FILE_APPEND | LOCK_EX);
	} else {
	echo $response;
	}
}
/* if(checkOAI($itemId)){
	
} */

function checkIdExistAndTrue($id){
	$db = new Database();
	$db->connect();
	$db->select('tbl_checker','itemId',NULL,'itemId='. $id,'itemId DESC'); // Table name, Column Names, JOIN, WHERE conditions, ORDER BY conditions
	$res = $db->getResult();
	if(count($res) > 0){
		echo count($res);
		return true;
	} else {
		return false;
	}
	
}

function insertItemId($id){
	$db = new Database();
	$db->connect();
	$db->insert('tbl_checker',array('itemId'=>$id,'triggeredNotif' => true));
}

?>