<?php
class DbConfig
{    
    private $_host = 'localhost';
    private $_username = 'root';
    private $_password = 'Welcome123!@2@2A2B@Team';
    private $_database = 'a2b_bknotif';
    
    protected $connection;
    
    public function __construct()
    {
        if (!isset($this->connection)) {
            
            $this->connection = new mysqli($this->_host, $this->_username, $this->_password, $this->_database);
            
            if (!$this->connection) {
				file_put_contents('dberror.log', 'Cannot connect to database server', FILE_APPEND | LOCK_EX);
                echo 'Cannot connect to database server';
                exit;

            }            
        }    
        
        return $this->connection;
    }
}
?>