<?php
//Enable error display
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
set_time_limit(0);

$_SERVER['DOCUMENT_ROOT'] = "C:/xampp/htdocs";
include_once $_SERVER['DOCUMENT_ROOT']."/hook/classes/cls-constant.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hook/api/podio-php-4.3.0/PodioAPI.php";

Podio::$debug = true;
$file = "err";

Podio::setup(Cons::CLIENT_ID, Cons::CLIENT_SECRET);

if($_POST){
	switch ($_POST['type']) {
			case 'hook.verify':
				PodioHook::validate($_POST['hook_id'], array('code' => $_POST['code']));
				break;
			case 'item.create':
				 $json_str = file_get_contents('php://input');
				 file_put_contents('postcreate'.date('Ymdhis').'.log',$json_str, FILE_APPEND | LOCK_EX);
				 $itemId = $_POST['item_id'];
				// $revisionId = 1;
				nsPost($itemId);
				break;
	}
} 

function nsPost($itemId){
	
	$app_id = "16627462";
	$app_token = "ac709f67e02c4cc1a1c6ce964628f2da";
	Podio::authenticate_with_app($app_id, $app_token);
	
	try{
		$items = PodioItem::get( $itemId );
		$requestInfo = $items->fields['email-body']->values;
		$data = array();
		$data = array(
			"requestInfo" => mb_strimwidth(strip_tags($requestInfo), 0, 300),
			"assignedTo" => isset($items->fields['assigned-t']->values[0]->name) ? $items->fields['assigned-t']->values[0]->name : "",
			"oaiTitle" => $items->title,
  			"reviewer" => isset($items->fields['reviewer']->values[0]->name) ? $items->fields['reviewer']->values[0]->name : "",
			"status" => $items->fields['status']->values[0]['text'],
			"link" => $items->link 
		);
		$data = json_encode($data);
		sendToOdd($data);
		file_put_contents('post'.date('Ymdhis').'.log',$data, FILE_APPEND | LOCK_EX);
	} catch(Exception $e) {
		file_put_contents('error.log', $e->getMessage(), FILE_APPEND | LOCK_EX);
	}
}

//nsPost(1320143741);
function sendToOdd($data){
	$postData = $data;
	$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_PORT => "1337",
	CURLOPT_URL => "http://localhost:1337/api/ns-post",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 120,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS => $postData,
	CURLOPT_HTTPHEADER => array(
		"cache-control: no-cache",
		"content-type: application/json",
		"postman-token: 262c4c0c-2b80-f794-f9a0-13f0d8a9520e"
	),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
	echo "cURL Error #:" . $err;
	file_put_contents('curlerror.log', $err, FILE_APPEND | LOCK_EX);
	} else {
	echo $response;
	}
}

?>